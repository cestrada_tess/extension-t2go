/* 
https://t2go.herokuapp.com/work-log

Necesitas estar en esta pÃ¡gina, ya logeado, abres la consola y pegas el script.

*/

(function () {
    let codigoAgente = localStorage.agentCode, dominio = localStorage.baseurl, token = localStorage.access_token,
        tokenKey = localStorage.tokenkey, datos

    const createTable = (title, data) => {
        let table = document.createElement('table');
        table.id = Math.ceil(Math.random(1) * 100)
        let tableBody = document.createElement('tbody');
        let tHead = document.createElement('thead')
        let headRows = document.createElement('tr')

        headRows.appendChild(createTd('Categoría'))
        headRows.appendChild(createTd('Horas'))
        tHead.appendChild(headRows)
        table.appendChild(tHead)

        function createTd(text) {
            let node = document.createElement('td');
            node.appendChild(document.createTextNode(text));
            return node
        }

        data.forEach(dato => {
            let row = document.createElement('tr');
            console.log('dat ', dato)
            for (let cellData in dato) {
                row.appendChild(createTd(cellData));
                row.appendChild(createTd(dato[cellData]));
            }

            tableBody.appendChild(row);
        });

        table.appendChild(tableBody);

        insertTable(table, table.id)
        return table;
    }
    const calculateHours = (data, current = false) => {
        let currMonth = parseInt(new Date().getMonth())

        let filteredData = datos.filter(dato => (dato.status == 'Aprobado' || current) && currMonth + current == parseInt(dato.workDate.split('-')[1]))

        let Categorias = filteredData.map(dato => dato.category).filter((elem, index, arr) => arr.indexOf(elem) === index)

        let rptCategorias = []

        Categorias.forEach(ele => {
            rptCategorias.push({
                [ele]: filteredData.filter(dato => dato.category == ele).map(x => x.hours).reduce((a, b) => a + b)
            })
        })

        return rptCategorias
    }
    const insertTable = (table, id) => {
        console.log('tabla', table)
        let app = document.querySelector('[ng-app=app]')
        let navbar = document.querySelector('navbar')
        let newTable = document.createElement('div')
        newTable.innerHTML = table
        app.insertBefore(table, navbar.nextSibling)

        console.log("prub", $('#' + id))
        setTimeout(() => $('#' + id).DataTable(), 1000)

    }
    const handleResponse = (res, par1, par2) => {
        console.log('Mes anterior: ')
        let past = calculateHours(res, 0)
        past.forEach(cat => console.log(cat))
        console.log(createTable('title', past))
        console.log('Mes Actual: ')
        let curr = calculateHours(res, 1)
        console.log(createTable('title', curr))
        curr.forEach(cat => console.log(cat))
    }

    $.ajax({
        type: "GET",
        url: `${dominio}/services/apexrest/t2go1/RegistroTrabajo/?codigoagente=${codigoAgente}&todoslosregistros=1`,
        headers: {
            "Authorization": tokenKey
        }
    }).done((res, msj) => datos = res)
        .then(res => handleResponse(res))
        .catch(e => console.log(e))
})()